# JackpotAffine

[> Accéder à la version numérique du jeu <](https://degrangem.forge.apps.education.fr/JackpotAffine/)


JackpotAffine est un jeu sérieux permettant de travailler les notions de coefficients directeurs et d'ordonnées à l'origine d'une fonction affine.

Dans ce jeu, les joueurs doivent essayer d'obtenir un jackpot en obtenant trois fois le même nombre.

![image](https://forge.apps.education.fr/DegrangeM/JackpotAffine/-/raw/main/static/assets/banniere.png){height=200px}

## Règles du jeu
Les règles du jeu sont disponibles à cette adresse :
https://forge.apps.education.fr/DegrangeM/JackpotAffine/-/raw/main/static/assets/r%C3%A8gles.pdf

## Version Papier (Print&Play)
Ce jeu est également disponible au format papier. Imprimez la règle ci-dessus ainsi que [les cartes](https://forge.apps.education.fr/DegrangeM/JackpotAffine/-/raw/main/static/assets/print_and_play.pdf?ref_type=heads) en recto-verso.

## Licence
Les règles et le matériel du jeu sont disponibles sous licence CC-BY 4.0. La version numérique du jeu est disponible sous licence MIT.

## Remarques
- Le jeu peut être joué en solo (auquel cas il faut essayer de faire le meilleure score) ou à plusieurs.
- Ce jeu est inspiré du jeu [Méga Jackpot](https://www.tikieditions.com/mega-jackpot/).
- Le jeu est développé à l'aide de SvelteKit.