#import "@preview/cetz:0.2.0": canvas, plot, draw
#import "template.typ": *
#import "@preview/gentle-clues:0.4.0": *

#show: project.with(
  title: [JackpotAffine - Règles],
  authors: (
    "Mathieu Degrange",
  ),
)

#align(center)[
_Un jeu réalisé par Mathieu Degrange et disponible sous licence CC-BY 4.0. 
_ #image("CCBY.svg", height: 0.75cm)
]

*JackpotAffine* est un jeu de société permettant de travailler les notions de coefficient directeur et d'ordonnée à l'origine d'une fonction affine. Il peut être joué seul ou à plusieurs.

= Matériel


- 128 cartes avec la courbe représentative d'une fonction affine d'un côté et un nombre de l'autre
  - 64 cartes dont le nombre correspond au coefficient directeur de la fonction affine
  - 64 cartes dont le nombre correspond à l'ordonnée à l'origine de la fonction affine
  - Les coefficients directeurs et ordonnées à l'origine peuvent prendre pour valeur -4, -3, -2, -1, 1, 2, 3 ou 4.

= Mise en place

- Mélanger les cartes et former trois tas (le premier avec 42 cartes, les deux autres avec 43 cartes) face fonction affine sur le dessus. Ces trois tas doivent être alignés horizontalement.
- Pour chaque tas, prendre la carte du dessus, la retourner, et la mettre en dessous sur la table.

#align(center,
  box([
      #canvas({
        draw.rect((0,0),(0.75,1), radius:10%, name:"D1");
        draw.rect((1,0),(1.75,1), radius:10%, name:"D2");
        draw.rect((2,0),(2.75,1), radius:10%, name:"D3");
        draw.rect((0,1.25),(0.75,2.25), radius:10%, name:"P1");
        draw.rect((1,1.25),(1.75,2.25), radius:10%, name:"P2");
        draw.rect((2,1.25),(2.75,2.25), radius:10%, name:"P3");
        draw.rect((0,1.25-0.05),(0.75,2.25), radius:10%);
        draw.rect((1,1.25-0.05),(1.75,2.25), radius:10%);
        draw.rect((2,1.25-0.05),(2.75,2.25), radius:10%);
        draw.grid((0.15,1.25+0.125), (0.6,1.25+0.875), stroke: gray, step: .15)
        draw.line((0.6,1.25+0.125+.15),(0.15,1.25+0.875-.15))
        draw.grid((1.15,1.25+0.125), (1.6,1.25+0.875), stroke: gray, step: .15)
        draw.line((1.15,1.25+0.125+.15),(1.6,1.25+0.875-.15))
        draw.grid((2.15,1.25+0.125), (2.6,1.25+0.875), stroke: gray, step: .15)
        draw.line((2.15,1.25+0.125+.15),(2.6,1.25+0.875-.15))
        draw.content("D1", [3]);
        draw.content("D2", [-2]);
        draw.content("D3", [4]);
      })
    ], baseline:45%)
)
  
#pagebreak()

= Règles du jeu

Derrière chaque carte fonction affine se trouve soit son coefficient directeur, soit son ordonnée à l'origine. Le but du jeu est d'essayer d'obtenir un jackpot en obtenant trois fois le même nombre. Les joueurs jouent chacun leur tour.


== Déroulement d'un tour


#set text(10pt)

#task(title:"1 - Choix d'une ou deux cartes")[
  Le joueur choisi *une pioche et une défausse* OU *deux pioches et deux défausses*.
  Pour chaque pioche choisi, il prend la carte du dessus et la pose sans la retourner sur la défausse choisie (qui n'est pas forcément celle située en dessous).

  #set align(center)
  #scale(80%,[#box([
    #canvas({
      draw.rect((0,0),(0.75,1), radius:10%, name:"D1");
      draw.rect((1,0),(1.75,1), radius:10%, name:"D2");
      draw.rect((2,0),(2.75,1), radius:10%, name:"D3");
      draw.rect((0,1.25),(0.75,2.25), radius:10%, name:"P1");
      draw.rect((1,1.25),(1.75,2.25), radius:10%, name:"P2");
      draw.rect((2,1.25),(2.75,2.25), radius:10%, name:"P3");
      
    draw.line("P1.center","D2.center", fill:black, mark: (end: ">"))
    })
  ], baseline:45%) #h(1cm) OU #h(1cm)#box([
    #canvas({
      draw.rect((0,0),(0.75,1), radius:10%, name:"D1");
      draw.rect((1,0),(1.75,1), radius:10%, name:"D2");
      draw.rect((2,0),(2.75,1), radius:10%, name:"D3");
      draw.rect((0,1.25),(0.75,2.25), radius:10%, name:"P1");
      draw.rect((1,1.25),(1.75,2.25), radius:10%, name:"P2");
      draw.rect((2,1.25),(2.75,2.25), radius:10%, name:"P3");
      draw.line("P2.center","D1.center", fill:black, mark: (end: ">"))
      draw.line("P3.center","D3.center", fill:black, mark: (end: ">"))
    });
  ], baseline:45%)], reflow:true);
]

#task(title:"2 - Révélation")[
  Le joueur retourne la ou les deux cartes choisies sur leur face nombre.
   #set align(center)
  #scale(80%,[#box([
    #canvas({
      draw.rect((0,0),(0.75,1), radius:10%, name:"D1");
      draw.rect((1,0),(1.75,1), radius:10%, name:"D2");
      draw.rect((2,0),(2.75,1), radius:10%, name:"D3");
     // draw.rect((0,1.25),(0.75,2.25), radius:10%, name:"P1");
     // draw.rect((1,1.25),(1.75,2.25), radius:10%, name:"P2");
     //  draw.rect((2,1.25),(2.75,2.25), radius:10%, name:"P3");
      draw.content("D1", [2]);
      draw.grid((1.15,0.125), (1.6,0.875), stroke: gray, step: .15)
      draw.line((1.15,0.125+.15),(1.6,0.875-.15))
      draw.content("D3", [2]);
      draw.arc-through((1.37,-0.1),(1.375,-0.5),(1.38,-0.1), mark: (end: ">", fill:black, scale:0.5))
    })
  ], baseline:45%)], reflow:true)
]

#task(title:"3 - Résultat")[
  Si les trois nombres sont égaux, alors c'est le jackpot ! Le joueur récupère les trois cartes aux nombres identiques. Cela correspond aux trois points gagnés.

   #set align(center)
  #scale(80%, [#box([
    #canvas({
      draw.rect((0,0),(0.75,1), radius:10%, name:"D1");
      draw.rect((1,0),(1.75,1), radius:10%, name:"D2");
      draw.rect((2,0),(2.75,1), radius:10%, name:"D3");
     // draw.rect((0,1.25),(0.75,2.25), radius:10%, name:"P1");
     // draw.rect((1,1.25),(1.75,2.25), radius:10%, name:"P2");
     //  draw.rect((2,1.25),(2.75,2.25), radius:10%, name:"P3");
      draw.content("D1", [2]);
      draw.content("D2", [2]);
      draw.content("D3", [2]);
      draw.content((5,0.5), [#set text(20pt); = 3 points])
      draw.content((5.5,-0.5), [#set text(20pt); #emoji.hand.splay], name:"hand")
      draw.line("D1.south", "hand.west", fill:black, mark: (end: ">"))
      draw.line("D2.south", "hand.west", fill:black, mark: (end: ">"))
      draw.line("D3.south", "hand.west", fill:black, mark: (end: ">"))

    })
  ], baseline:45%)], reflow:true) 
  
  _Si après avoir récupéré ces cartes certaines défausses sont vides, on prend une carte de la pioche située au dessus et on la met sur la défausse face nombre visible._
]

#task(title:"4 - Relance (facultatif)")[
  En cas d’*échec*, le joueur peut, s'il le souhaite et s'il en a les moyens, dépenser un point pour rejouer (en recommençant à l'étape 1). Il défausse pour cela une de ses cartes.

  Un joueur ne peut effectuer qu'une seule relance par tour.
]

#idea(title:"Règle de défausse")[
  Si le joueur n'a choisi qu'une seule pioche à l'étape 1, alors le joueur suivant peut (s'il le souhaite) défausser une carte fonction au début de son tour. En cas de relance, seule la dernière action compte.
]

#set text(11pt)

== Fin du jeu

Le jeu se termine lorsque qu'une des trois pioches est vide. Le joueur termine son tour. Le gagnant est le joueur avec le plus de points.
