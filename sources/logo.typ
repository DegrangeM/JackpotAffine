#import "@preview/cetz:0.2.0": canvas, plot, draw
#import "@preview/gentle-clues:0.4.0": *

#set page(/*flipped:true, */width:5cm, height:5cm, margin: (left: 0mm, right: 0mm, top: 0mm, bottom: 0mm))
#set text(font: "Imposible", lang: "fr")
#set par(justify: true)


#let fonctions = ()


#fonctions.push({
  place(center+horizon, dy:-1.25mm, dx:-0.5mm, scale(x:100%,y:100%,[
      #set text(84pt)
      JA
    ]))
  place(image("fond.png", width: 47mm), dx:1.5mm,dy:1.5mm)
})

#table(
  columns: (5cm,)*1,
  rows: (5cm,)*1,
  inset: 0pt,
  stroke: 3mm  + black,
  align: center+horizon,
  ..fonctions,
)