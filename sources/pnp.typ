#import "@preview/cetz:0.2.0": canvas, plot, draw
#import "@preview/gentle-clues:0.4.0": *

#set page(/*flipped:true, */margin: (left: 5mm, right: 5mm, top: 5mm, bottom: 5mm))
#set text(font: "Linux Libertine", lang: "fr")
#set par(justify: true)

// Pour la prochaine fois : créer un array contenant une liste d'objet avec un type associé et utiliser cet array pour générer automatiquement tous les dos de cartes

// 450/1.968504 = 228.59999
// /2 = 114.299996342


#let cartes = ()

#let mm = (-4,-3,-2,-1,1,2,3,4)
// #let mm = (-4,-3)
#let pp = (-4,-3,-2,-1,1,2,3,4)

#let carteJeu(m, p, n) = (
      recto: {
        place(center+horizon, dy:-1.25mm, scale(x:55%,y:55%,canvas(length: 1.5cm, {
          plot.plot(size: (3.5*0.8, 3.5),
            x-tick-step: 1,
            y-tick-step: 1,
            axis-style:"school-book",
            x-grid:true,
            y-grid:true,
            x-min:-4,
            x-max:4,
            y-min:-5,
            y-max:5,
            x-label:"",//
            y-label:"", //$f(x)$,
            {
              plot.add(
                style: (stroke: 1.5pt + red),
                domain: (-5, 5), x=> m*x+p)
            })
        })))
        place(image("fond.png", width: 47mm), dx:1.5mm,dy:1.5mm)
      },
      verso: {
          place(center+horizon, dy:-1.25mm, scale(x:100%,y:100%,[
            #set text(64pt)
            #n
          ]))
        place(image("fond.png", width: 47mm), dx:1.5mm,dy:1.5mm)
      }
    )

#for m in mm { // Se limiter à -3;3 ?
  for p in pp {
    cartes.push(carteJeu(m,p,m))
    cartes.push(carteJeu(m,p,p))
  }
}

// #let output = ()
#for n in range(calc.ceil(cartes.len()/(5*4))) {
  table(
  columns: (5cm,)*4,
  rows: (5cm,)*5,
  inset: 0pt,
  stroke: 3mm  + black,
  align: center+horizon,
  ..cartes.slice(n*20,calc.min((n+1)*20, cartes.len())).map(x=>x.recto),
  )
  let dos = ()
  for j in range(5) {
    for i in range(4) {
      dos.push(cartes.at(20*n+4*j+(3-i),default:(verso:[])).verso)
    }
  }
  pagebreak()
  table(
  columns: (5cm,)*4,
  rows: (5cm,)*5,
  inset: 0pt,
  stroke: 3mm  + black,
  align: center+horizon,
  ..dos,
  )
  pagebreak(weak:true)

}

/*
#table(
  columns: (5cm,)*4,
  rows: (5cm,)*5,
  inset: 0pt,
  stroke: 3mm  + black,
  align: center+horizon,
  ..cartes.map(x=>x.recto),
)
*/