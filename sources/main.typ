#import "@preview/cetz:0.2.0": canvas, plot, draw
#import "@preview/gentle-clues:0.4.0": *

#set page(/*flipped:true, */width:40cm, height:40cm, margin: (left: 0mm, right: 0mm, top: 0mm, bottom: 0mm))
#set text(font: "Linux Libertine", lang: "fr")
#set par(justify: true)

// Pour la prochaine fois : créer un array contenant une liste d'objet avec un type associé et utiliser cet array pour générer automatiquement tous les dos de cartes

// 450/1.968504 = 228.59999
// /2 = 114.299996342


#let fonctions = ()

#let mm = (-4,-3,-2,-1,1,2,3,4)
// #let mm = (-4,-3)
#let pp = (-4,-3,-2,-1,1,2,3,4)


#for m in mm { // Se limiter à -3;3 ?
  for p in pp {
    
    fonctions.push({
      place(center+horizon, dy:-1.25mm, scale(x:55%,y:55%,canvas(length: 1.5cm, {
      plot.plot(size: (3.5*0.8, 3.5),
        x-tick-step: 1,
        y-tick-step: 1,
        axis-style:"school-book",
        x-grid:true,
        y-grid:true,
        x-min:-4,
        x-max:4,
        y-min:-5,
        y-max:5,
        x-label:"",//
        y-label:"", //$f(x)$,
        {
          plot.add(
            style: (stroke: 1.5pt + red),
            domain: (-5, 5), x=> m*x+p)
        })
    })))
    place(image("fond.png", width: 47mm), dx:1.5mm,dy:1.5mm)
  })
  }
}

#table(
  columns: (5cm,)*8,
  rows: (5cm,)*8,
  inset: 0pt,
  stroke: 3mm  + black,
  align: center+horizon,
  ..fonctions,
)