#import "@preview/cetz:0.2.0": canvas, plot, draw
#import "@preview/gentle-clues:0.4.0": *

#set page(/*flipped:true, */width:40cm, height:5cm, margin: (left: 0mm, right: 0mm, top: 0mm, bottom: 0mm))
#set text(font: "Imposible", lang: "fr")
#set par(justify: true)

// Pour la prochaine fois : créer un array contenant une liste d'objet avec un type associé et utiliser cet array pour générer automatiquement tous les dos de cartes

// 450/1.968504 = 228.59999
// /2 = 114.299996342


#let fonctions = ()

#let mm = (-4,-3,-2,-1,1,2,3,4)


#for m in mm { // Se limiter à -3;3 ?    
    fonctions.push({
      place(center+horizon, dy:-1.25mm, scale(x:100%,y:100%,[
        #set text(64pt)
        #m
      ]))
    place(image("fond.png", width: 47mm), dx:1.5mm,dy:1.5mm)
  })
}

#table(
  columns: (5cm,)*8,
  rows: (5cm,)*1,
  inset: 0pt,
  stroke: 3mm  + black,
  align: center+horizon,
  ..fonctions,
)